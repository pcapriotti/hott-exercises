## HoTT exercises

[![build status](https://gitlab.com/pcapriotti/hott-exercises/badges/master/build.svg)](https://gitlab.com/pcapriotti/hott-exercises/commits/master)

Solutions of the exercises of the [HoTT book](http://homotopytypetheory.org/book/).

Browsable version: http://pcapriotti.gitlab.io/hott-exercises
