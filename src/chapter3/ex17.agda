{-# OPTIONS --without-K #-}
module chapter3.ex17 where

open import sum
open import hott
open import equality

ind : ∀ {i j}{A : Set i}
    → (P : Trunc 1 A → Set j)
    → ((x : Trunc 1 A) → h 1 (P x))
    → ((x : A) → P [ x ])
    → (x : Trunc 1 A) → P x
ind {A = A} P hP f x = subst P (h1⇒prop (Trunc-level 1) _ _) (proj₂ (f' x))
  where
    S : Set _
    S = Σ (Trunc 1 A) P

    hS : h 1 S
    hS = Σ-level (Trunc-level 1) hP

    f' : Trunc 1 A → S
    f' = Trunc-elim 1 _ _ hS λ x → [ x ] , f x
