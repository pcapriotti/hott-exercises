{-# OPTIONS --without-K #-}
module chapter3.ex19 where

open import sum
open import decidable
open import equality
open import hott
open import sets.core
open import sets.nat
open import sets.fin
  hiding (_<_; compare)
open import sets.empty

module _ {i}(P : ℕ → Set i)
         (hP : (n : ℕ) → h 1 (P n))
         (P? : (n : ℕ) → Dec (P n)) where
  None : ℕ → Set _
  None n = ∀ j → j < n → ¬ P j

  None' : ℕ → Set _
  None' n = ∀ j → j ≤ n → ¬ P j

  lt-leq : ∀ {n m} → n ≤ m → n < m ⊎ n ≡ m
  lt-leq {m = zero} z≤n = inj₂ refl
  lt-leq {m = suc m} z≤n = inj₁ (s≤s z≤n)
  lt-leq (s≤s q) with lt-leq q
  lt-leq (s≤s q) | inj₁ p = inj₁ (s≤s p)
  lt-leq (s≤s q) | inj₂ e = inj₂ (ap suc e)

  none-h1 : ∀ n → h 1 (None n)
  none-h1 n = Π-level λ j
            → Π-level λ p
            → Π-level λ _
            → ⊥-prop

  none'-h1 : ∀ n → h 1 (None' n)
  none'-h1 n = Π-level λ j
             → Π-level λ p
             → Π-level λ _
             → ⊥-prop

  none-none' : ∀ {n} → None n → ¬ P n → None' n
  none-none' {n} none u j q p with lt-leq q
  none-none' none u j _ p | inj₁ q = none _ q p
  none-none' none u n q p | inj₂ refl = u p

  none-cons : ∀ {n} → None n → ¬ P n → None (suc n)
  none-cons none u j (s≤s q) p = none-none' none u j q p

  data Result (n : ℕ) : Set i where
    found : (i : ℕ) → i < n
          → P i → None i
          → Result n
    not-found : None n → Result n

  IsMin : ℕ → Set _
  IsMin n = P n × None n

  is-min-h1 : (n : ℕ) → h 1 (IsMin n)
  is-min-h1 n = ×-level (hP n) (none-h1 n)

  Min : Set i
  Min = Σ ℕ IsMin

  min-eq : {n m : Min}
         → proj₁ n ≡ proj₁ m
         → n ≡ m
  min-eq {n , d}{m , d'} p = unapΣ (p , h1⇒prop (is-min-h1 m) _ _)

  min-prop : prop Min
  min-prop (n , d , u) (m , d' , u') with compare n m
  ... | lt q = ⊥-elim (u' n q d)
  ... | eq q = min-eq q
  ... | gt q = ⊥-elim (u m q d')

  find : (n : ℕ) → Result n
  find 0 = not-found (λ j ())
  find (suc n) with find n
  ... | found i q p u = found i (trans≤ q suc≤) p u
  ... | not-found u with P? n
  ... | yes r = found n (s≤s refl≤) r u
  ... | no v = not-found (none-cons u v)

  min' : Σ ℕ P → Min
  min' (n , p) with find n
  ... | found i q p' u = i , p' , u
  ... | not-found u = n , p , u

  min : Trunc 1 (Σ ℕ P) → Min
  min = Trunc-elim 1 _ _ (prop⇒h1 min-prop) min'

  markov : Trunc 1 (Σ ℕ P) → Σ ℕ P
  markov z = let (n , d , u) = min z in n , d
