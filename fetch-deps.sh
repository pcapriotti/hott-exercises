#!/bin/bash
set -e

function add_lib() {
    name="$PWD/$NAME/$NAME.agda-lib"
    if ! grep -q "$name" "$HOME/.agda/libraries"
    then
        echo "$name" >> "$HOME/.agda/libraries"
    fi
}

mkdir -p "$HOME/.agda"
touch "$HOME/.agda/libraries"

sed -n 's!^depend: !! p' hott-exercises.agda-lib |
  while IFS= read NAME; do
      git clone "https://pcapriotti:$GITLAB_TOKEN@gitlab.com/pcapriotti/$NAME"
      add_lib "$NAME"
  done
