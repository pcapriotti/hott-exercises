#!/bin/bash
set -e

function die() {
    echo "${RED}$1${NORMAL}" >&2
    usage
}

function usage() {
    echo "Usage: $0 -u URL -b BRANCH -t TOKEN"
    exit 1
}

function gitlab_ci_build_repo() {
    url="$CI_BUILD_REPO"
    if [ ! -z "$url" ]; then
        # use appropriate deploy token
        url=$(echo "$url" |
            sed 's!^https://gitlab-ci-token:[^@]*!https://pcapriotti:'"$token"'|')
    fi

    echo "$url"
}

RED=""
GREEN=""
YELLOW=""
NORMAL=""

while getopts u:b:t: FLAG; do
    case $FLAG in
        u)
            url=$OPTARG
            ;;
        b)
            branch=$OPTARG
            ;;
        t)
            token=$OPTARG
            ;;
    esac
done

[ -z "$branch" ] && branch="$CI_BUILD_REF_NAME"
[ -z "$branch" ] && branch=$(git rev-parse --abbrev-ref HEAD)
[ -z "$branch" ] && die "No branch given"

[ -z "$token" ] && token=$GITLAB_TOKEN
[ -z "$token" ] && die "Deploy token not set. Please set \$GITLAB_TOKEN variable."

[ -z "$url" ] && url=`gitlab_ci_build_repo`
if [ -z "$url" ]; then
    echo "${YELLOW}No url set. Pushing disabled.${NORMAL}"
fi

# clone pages website
html=public
rm -fr "$html"
git branch -f pages remotes/origin/pages || true
git clone -b pages . "$html"
rm -fr "$html"/"$branch"

# generate html
echo "${GREEN}Generating HTML${NORMAL}"
agda -i. --html --html-dir="$html"/"$branch" README.agda
mv "$html"/"$branch"/README.html "$html"/"$branch"/index.html

# push updated website
sha=`git log -1 HEAD --pretty=format:%H`
cd "$html"
git add -A
git config user.name "Paolo Capriotti"
git config user.email "paolo@capriotti.io"
git commit -m "Update $branch: $sha" || true

if [ ! -z "$url" ]; then
    git push "$url" pages:pages
fi

# cleanup
rm -fr .git
